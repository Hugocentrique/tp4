$(document).ready(function(){
    var prix,nbparts,tot,supl,pate;
    $('.pizza-type label').hover(
        function(){
            $(this).find(".description").show();
        },
        function(){
            $(this).find(".description").hide();
        }
    );

    $('.nb-parts input').on('keyup', function(){
        $(".pizza-pict").remove();

        var pizza = $('<span class="pizza-pict"></span>');

        slices = +$(this).val();

        for(i=0;i< slices/6; i++){
            $(this).after(pizza.clone().addClass('pizza-6'));
        }

        if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);

    })

    $('.next-step').click(function(){
        $(this).remove();
        $(".infos-client").show();
    });

    $('.add').click(function(){
        $(this).before('<br><input type="text"/>');
    });


    $('.done').click(function(){
        var prenom = $('.infos-client .type:first-child input').val();
        $('.typography-row').remove();
        $('.stick-right').remove();
        $(".container").append("Merci à toi   " +prenom +"<br><br>");
    });


    $(".pizza-type input").click(function(){
        prix=0;
        if("input[type='radio'][name='type']:checked"){
            
            prix = $(this).attr('data-price');
            prix = prix/6;
            $('.stick-right p').text(prix);
        }
    });

    $('.nb-parts input').on('keyup',function(){
       nbparts = $(this).val();
        tot = prix*nbparts;
        $('.stick-right p').text(tot);
    });
    
    


});